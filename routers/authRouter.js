const express = require('express');

const authRouter = express.Router();
const {
  createProfile,
  login,
} = require('../controllers/authService.js');

authRouter.post('/register', createProfile);

authRouter.post('/login', login);


module.exports = {
  authRouter
};
// export default authRouter
